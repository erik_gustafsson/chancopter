# ChanCOPter
## Run
Running the application is a two step process. First fire up sbt, then in the sbt prompt run the application using `run <AMOUNT>`
```
$ sbt
sbt:ChanCOPter> run <AMOUNT>
```
Note: sbt run <AMOUNT> will not work

## TODO (lots of things left out due to time contraints)
* Have a service layer that returns a Future[SomeResponse] so the solution could easily be made into a web service using Fintra,
while the command line utility would just block until the response is ready and then print it.
* Setup tests
* Better class/module structure but also nice to keep everything in one place because it's not too large.
* Memoize solution
* Write a better README

