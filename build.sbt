name := "ChanCOPter"

version := "0.1"

scalaVersion := "2.12.6"

mainClass in (Compile, run) := Some("com.magine.worktest.ChanCOPter")

libraryDependencies ++= Seq(
  "com.twitter" %% "finatra-httpclient" % "18.6.0"
)