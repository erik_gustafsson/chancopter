package com.magine.worktest

import com.twitter.finatra.json.FinatraObjectMapper

case class ChanCOPterResponseSummary(totalChannels: Int,
                                     totalUniqueChannels: Int,
                                     totalPrice: Int,
                                     uniqueChannelIds: List[Int])

case class ChanCOPterResponse(summary: ChanCOPterResponseSummary, productsToBuy: List[Product])

sealed trait ChanCOPterError {
  val message: String
}

case class InvalidAmount(message: String) extends ChanCOPterError

case class InvalidDataLoader(message: String)

object ChanCOPter {
  def main(args: Array[String]): Unit = {
    val mapper = FinatraObjectMapper.create()
    Parser.parse(args)
      .right.map({
      case (amount: Int, dataLoader: DataLoader) => {
        val channels = dataLoader.getProducts()

        val result = ChannelOptimizer.maxChannels(amount, channels)

        val totalChannels = result.products.flatMap(_.channelIds)
        val uniqueChannels = totalChannels.distinct
        val totalPrice = if(result.products.isEmpty) 0 else result.products.map(_.price).reduce(_ + _)
        assert(result.totalChannels == totalChannels.length)

        val summary = ChanCOPterResponseSummary(result.totalChannels, uniqueChannels.length, totalPrice, uniqueChannels)
        val response = ChanCOPterResponse(summary, result.products)
        println(mapper.writeValueAsString(response))
      }
    })
      .left.map(error => println(error.message))
  }
}
