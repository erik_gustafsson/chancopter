package com.magine.worktest

case class ChannelOptimizerResult(products: List[Product], totalChannels: Int)

object ChannelOptimizer {

  def maxChannels(amountToSpend: Int, products: List[Product]): ChannelOptimizerResult = {
    return maxChannels(amountToSpend, products, ChannelOptimizerResult(List(), 0))
  }

  private def maxChannels(amount: Int, products: List[Product], result: ChannelOptimizerResult): ChannelOptimizerResult =
  // TODO Memoize
    products match {
      case Nil => result
      case product :: productsLeftToProcess => {
        val withoutProduct = maxChannels(amount, productsLeftToProcess, result)
        if (amount - product.price >= 0) {
          val newResult =
            result.copy(products = product :: result.products, totalChannels = product.channelIds.length + result.totalChannels)
          val withProduct = maxChannels(amount - product.price, productsLeftToProcess, newResult)
          if (withoutProduct.totalChannels > withProduct.totalChannels)
            withoutProduct
          else
            withProduct
        } else {
          withoutProduct
        }
      }
    }
}
