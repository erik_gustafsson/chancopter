package com.magine.worktest

import com.fasterxml.jackson.annotation.JsonProperty
import com.twitter.finatra.httpclient.{HttpClient, RequestBuilder}
import com.twitter.finatra.json.FinatraObjectMapper
import com.twitter.util.Await

case class JsonResponse(products: List[Product])

case class Product(id: String, price: Int, name: String, @JsonProperty("channelIds") channelIds: List[Int])

trait DataLoader {
  def getProducts(): List[Product]
}

class FileDataLoader() extends DataLoader {
  override def getProducts(): List[Product] = {
    val lines = scala.io.Source.fromFile("src/main/resources/channel_packs.json").mkString
    val mapper = FinatraObjectMapper.create()
    val response = mapper.parse[JsonResponse](lines)
    response.products
  }
}

class HttpDataLoader(httpClient: HttpClient) extends DataLoader {
  override def getProducts(): List[Product] = {
    val request = RequestBuilder.get("/api/superscription/v1/market/de").request
    Await.result(httpClient.executeJson[JsonResponse](request)).products
  }
}

class HardCodedDataLoader() extends DataLoader {
  override def getProducts(): List[Product] =
    List(Product("id 1", 1, "One product, one cost", (List(1))),
      Product("id 2", 2, "Three products, two cost ", (List(1, 2, 3))),
      Product("id 3", 3, "Two products, three cost", (List(4, 5))),
      Product("id 4", 5, "Four products, four cost", (List(1, 2, 3, 4))))
}