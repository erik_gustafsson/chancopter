package com.magine.worktest

import com.twitter.finatra.httpclient.{HttpClient, RichHttpClient}
import com.twitter.finatra.json.FinatraObjectMapper

import scala.util.{Failure, Success, Try}

object Parser {
  def parse(args: Array[String]): Either[ChanCOPterError, (Int, DataLoader)] = {
    if (args.length == 0) {
      return Left(InvalidAmount("Usage: parameter amountToSpend missing, e.g. \n$ sbt\n > run 5500"))
    }
    val amountTry = Try(args(0).toInt) match {
      case Failure(exception) => Left(InvalidAmount("Amount must be an integer"))
      case Success(amount) if amount < 0 => Left(InvalidAmount("Amount must be postive"))
      case Success(amount) => Right(amount)
    }

    val dataLoader =
      if (args.length > 1) {
        args(1) match {
          case "hardcoded" => new HardCodedDataLoader()
          case "file" => new FileDataLoader()
          case "api" => httpDataLoader
          case _ => httpDataLoader
        }
      } else {
        httpDataLoader
      }
    amountTry.map(amount => (amount, dataLoader))
  }

  private def httpDataLoader = {
    val httpService = RichHttpClient.newSslClientService("magine.com", "magine.com:443")
    val mapper = FinatraObjectMapper.create()
    val httpClient = new HttpClient(httpService = httpService, mapper = mapper)
    new HttpDataLoader(httpClient)
  }
}